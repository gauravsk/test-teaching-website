---
author: Alison Hill
cascade:
  layout: single-series
description: "Sometimes you want a souped-up series- a bundle of related pages meant to be read in sequence. This section is like a blog series, minus the blog."
layout: list #list-sidebar
show_author_byline: false
show_post_date: false
show_post_thumbnail: true
subtitle: A collection, minus the blog.
thumbnail_left: false
title: Weekly activities
---
