---
author: Gaurav Kandlikar
categories:
- Syllabus
date: "2023-05-26"
excerpt: Stuff to test
layout: single
show_post_date: false
title: Course syllabus
---

***Day-to-day course schedule may change. Please check this website or the course Canvas for the most recent version!***

## Course overview

  Hi and welcome to Principles of Ecology! :smiley: <p>

<div class="row">
  <div class="column">

  My name is Dr. Gaurav Kandlikar (he/him/his), and I am so excited to share this class with you! This is my first year as a professor at LSU, and I'm really looking forward to get to know you all, Baton Rouge, and the nature in Louisiana. <p>
I study plant ecology, which means I study how plants interact with the environment and with other organisms in their environment. A lot of my work has especially focused on how plants interact with the soil microbiome. I love my job because it has given me the opportunity to travel to amazing places and meet amazing people from all over the world. In my free time I enjoy running/biking, cooking, listening to music, and observing birds.
  </div>
  <div class="column">
    <img src="GSK_profile.jpg" style="width:700px;">
  </div>
</div>

-----------------------



Stuff and things happen in this class. 


| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title       |
| Paragraph   | Text        |
| Header2      | Title       |
| Paragraph2   | Text        |
| Header3      | Title       |
| Paragraph3   | Text        |