---
author: 
cascade:
  author: Gaurav Kandlikar
  show_author_byline: false
  show_comments: false
  show_post_date: false
  show_post_time: false
  sidebar:
    show_sidebar_adunit: false
    text_link_label: View recent talks
    text_link_url: /talk/
description: |
layout: list
show_author_byline: false
show_button_links: true
show_post_date: false
show_post_thumbnail: true
show_post_time: false
show_post_location: false
title: ""
---

** No content below YAML for the talk _index. This file provides front matter for the listing page layout and sidebar content. It is also a branch bundle, and all settings under `cascade` provide front matter for all pages inside talk/. You may still override any of these by changing them in a page's front matter.**
