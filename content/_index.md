---
action_label: Read the syllabus &rarr;
action_link: /syllabus
action_type: text
description: Welcome to Principles of Ecology! <br> <br>
  I am thrilled that you are here, and I am excited to share this class with you for the next few months! Please navigate to the  [syllabus]() for an overview of our semester, or head directly to the [course materials](). 
image_left: false
images:
- img/lac2.jpg
show_action_link: true
show_social_links: false
subtitle: BIOL 4253 at Louisiana State University
text_align_left: false
title: Principles of Ecology
type: home
---

** index doesn't contain a body, just front matter above.
See index.html in the layouts folder **
