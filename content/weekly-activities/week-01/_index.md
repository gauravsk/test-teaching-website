---
author: Alison Hill
date: "2021-01-25"
description: |
  Our first day together is all about building and publishing.
excerpt: null
layout: single-series
publishDate: "2021-01-22"
show_author_byline: false
show_post_date: false
show_post_thumbnail: true
subtitle: All about Day 01 of 'Introduce Yourself Online'.
title: "Week 1: Welcome and course overview"
weight: 1
---

If you choose `layout: single-series`, you can add markdown text here and it will be the landing page for this nested subsection. 
trdt